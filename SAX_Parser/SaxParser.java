import org.w3c.dom.Attr;
import org.xml.sax.*;
import org.xml.sax.helpers.*;
import javax.xml.parsers.*;
import java.io.*;

public class SaxParser implements ContentHandler {
  int level=-1;
  Elem el;
  LList queue = new LList();

  public static boolean toBeIgnored(String str){
		char[] cont = str.toCharArray();
		boolean retVal=true;
		for(int i=0; i<cont.length; i++){
			if(!(cont[i]=='\n' || cont[i]=='\t' || cont[i]==' ' || cont[i]=='\r'))	{
				retVal=false;
				break;
			}
		}
		return retVal;
	}

  public static void main(String[] args) throws Exception {    
    SaxParser xmlep = new SaxParser();
    xmlep.go(args);
  }

  public void go(String[] args)
  { 
    String file=null;   
    try{
      if(args.length==0){
        System.out.println("Please, specify the xml file you want to parse.");
      }
      else{
        for(int f=0;f<args.length;f++){
          level=-1;
          file = args[f];   
          SAXParserFactory spf = SAXParserFactory.newInstance();
          SAXParser sp = spf.newSAXParser();
          XMLReader sr = sp.getXMLReader();
          sr.setContentHandler(this);
          sr.parse(file);
          String tempstr = "+++ We begin parsing file "+file+". Here are the results. +++";
          String per ="";
          for(int i=0; i<tempstr.length();i++){
              per=per+"+";
          }
          System.out.println('\n'+per); 
          System.out.println(tempstr);
          System.out.println(per);            


          //at this moment we have finished reading the XML and we procceed to print it in levels
          while(!queue.isEmpty()){   
            el=queue.dequeue();
            if(el.level>level){ //we reached new level
              level++;
              String str = "|| Level "+level+" ||";
              String fill ="";
              for(int i=0; i<str.length();i++){
                  fill=fill+"=";
              }
              System.out.println('\n'+fill);
              System.out.println(str);
              System.out.println(fill+'\n');
            }
            if(el.NodeName!=null){ // "element" that contains attributes of parent node
              if(el.allAttr!=null){
                System.out.println("Element \""+el.NodeName+"\" has attributes:");
                StrElem curr = el.allAttr.head;
                for (int i = 0; i < el.allAttr.length; i++) {
                  System.out.println("-> " + curr.LocalName +"="+curr.attr);
                  curr=curr.next;
                }
              }
              else{
                System.out.println("Element \""+el.NodeName+"\"");
              }
            }
            else { //this is a text node
              System.out.println("Text: "+el.text);
            }    
          }
        }
      }
    }
    catch (IOException|IllegalArgumentException ioe){
      if(file != null){
          System.out.println("File \""+file+"\" could not be opened.");
      }
      else{
          System.out.println("IO Exception happened.");
      }
      System.out.println("Program will be terminated.");
    }
    catch (Exception e){
      e.getMessage();
      System.out.println("SOMETHING WENT WRONG!");
    }
  }

  // Implementations of ALL ContentHandler interface methods:

  public void setDocumentLocator(Locator locator) {
  }

  public void startDocument() throws SAXException {
  }

  public void endDocument() throws SAXException {
  }

  public void startPrefixMapping(String prefix, String uri) throws SAXException {
  }

  public void endPrefixMapping(String prefix) throws SAXException {
  }

  public void startElement(String namespaceURI, String localName, String qName, Attributes atts) throws SAXException {
    if (atts.getLength() > 0) {
      MyAttributes myAttr = new MyAttributes(atts.getLength());
      StrElem strElem,curr;
      strElem = new StrElem(atts.getLocalName(0), atts.getValue(0));
      myAttr.head=strElem;
      curr=strElem;
      for (int i = 1; i < atts.getLength(); i++) {
        strElem = new StrElem(atts.getLocalName(i), atts.getValue(i));
        curr.next=strElem;
        curr=strElem;
      }

      el = new Elem(qName, myAttr, ++level);
    }
    else el = new Elem(qName, null, ++level);
    queue.enqueue(el);
  }

  public void endElement(String namespaceURI, String localName, String qName) throws SAXException {    
    level--;
  }

  public void characters(char ch[], int start, int lenght) throws SAXException {
    String str="";
    for(int i=start; i<lenght; i++){
      if(ch[i]!='\n' && ch[i]!='\t' && ch[i]!='\r')
        str+=ch[i];
    }
    if(!toBeIgnored(str)) {
      el = new Elem(str, level+1);
      queue.enqueue(el);
    }
  }

  public void ignorableWhitespace(char ch[], int start, int lenght) throws SAXException {
  }

  public void processingInstruction(String target, String data) throws SAXException {
  }

  public void skippedEntity(String name) throws SAXException {
  }
}