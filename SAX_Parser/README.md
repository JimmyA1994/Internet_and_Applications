This is an SAX parser that accepts multiple xml files and prints the structure (elements and attributes) of each xml file by it's tree depth. For instance, level 0 is the root element and every next level corresponds to a deeper level. The text of an element is considered to be a level below the element that encloses it. SAX is event-driven so we need to store all the xml elements and attributes before we start printing them, in order to correctly represent the XML structure.
# Example run:
~~~~
javac SaxParser.java
java SaxParser Examples/Cars.xml Examples/simple.xml Examples/plant_catalog.xml
~~~~
