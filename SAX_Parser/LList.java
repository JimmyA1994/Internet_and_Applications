class LList {
    Elem head;
    int length;

    LList(){
        this.head=null;
        this.length=0;
    }

    Elem peak(){
        return head;
    }

    void enqueue(Elem x){ // sorted insertion
        this.length++;
        if(isEmpty()){
            head=x;
            return;
        }
        Elem curr=head;
        while(true){
            if(curr.next==null){
                curr.next=x;
                return;
            }
            else if(curr.next.level>x.level){
                x.next=curr.next;
                curr.next=x;
                return;
            }
            curr=curr.next;
        }        
    }

    Elem dequeue(){
        Elem temp=null;        
        if(!isEmpty()){    
            temp=head;      
            this.length--;
            head=head.next;
        }
        return temp;
    }

    boolean isEmpty(){
        return head==null;
    }
}