import org.xml.sax.*;

class Elem {
    String NodeName;
    MyAttributes allAttr;
    String text;
    int level;
    Elem next;
    
    Elem (String str, MyAttributes atts, int l) {
        this.NodeName=str;
        this.allAttr = atts;
        this.text=null;
        this.level=l;
        this.next=null;
    }

    Elem (String txt, int l) {
        this.NodeName=null;
        this.allAttr = null;
        this.text=txt;
        this.level=l;
        this.next=null;
    }
}