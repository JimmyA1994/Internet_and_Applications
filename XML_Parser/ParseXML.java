import org.w3c.dom.*;
import javax.xml.parsers.*;
import java.io.*;
public class ParseXML{

    public static boolean toBeIgnored(String str){
		char[] cont = str.toCharArray();
		boolean retVal=true;
		for(int i=0; i<cont.length; i++){
			if(!(cont[i]=='\n' || cont[i]=='\t' || cont[i]==' ' || cont[i]=='\r'))	{
				retVal=false;
				break;
			}
		}
		return retVal;
	}
	public static void main(String[] args) {
        String file=null;
		try{
            if(args.length==0){
                System.out.println("Please, specify the xml file you want to parse.");
            }
            else{
                for(int f=0;f<args.length;f++){
                    file = args[f];              
                    DocumentBuilderFactory fact = DocumentBuilderFactory.newInstance();
                    DocumentBuilder builder= fact.newDocumentBuilder();
                    Document doc = builder.parse(file);
                    String tempstr = "+++ We begin parsing file "+file+". Here are the results. +++";
                    String per ="";
                    for(int i=0; i<tempstr.length();i++){
                        per=per+"+";
                    }
                    System.out.println('\n'+per); 
                    System.out.println(tempstr);
                    System.out.println(per);  

                    //initialization of queue
                    Node root = doc.getDocumentElement();
                    FIFO queue = new FIFO();                    
                    int level = -1;
                    int num;
                    Elem el;
                    NodeList children;
                    el = new Elem(root,level+1);
                    queue.enqueue(el);

                    //implementation of breadth-first search
                    while(!queue.isEmpty()){            
                        el=queue.dequeue();

                        if(el.level>level){ //we reached new level
                            level++;
                            String str = "|| Level "+level+" ||";
                            String fill ="";
                            for(int i=0; i<str.length();i++){
                                fill=fill+"=";
                            }
                            System.out.println('\n'+fill);
                            System.out.println(str);
                            System.out.println(fill+'\n');
                        }                
                            
                        //printing info
                        if(el.node.getNodeType()==Node.ELEMENT_NODE){ 
                            NamedNodeMap allAttr = el.node.getAttributes();                                                   
                            if(allAttr.getLength()>0){
                                System.out.println("Element \""+el.node.getNodeName()+"\" has attributes:");
                                for ( int k = 0 ; k < allAttr.getLength() ; k++ ) { 
                                    System.out.println("-> "+allAttr.item(k).getNodeName() + " = " + allAttr.item(k).getNodeValue());
                                }
                            }
                            else{
                                System.out.println("Element \""+el.node.getNodeName()+"\"");
                            }
                        }
                        else if(el.node.getNodeType()==Node.TEXT_NODE){   
                            String text = el.node.getNodeValue().replace("\n", "").replace("\r", "");                     
                            System.out.println("Text: "+text);
                            continue; //text nodes don't have children
                        }             
                        //add node's children to the queue
                        children=el.node.getChildNodes();
                        num=children.getLength();
                        for(int i=0; i<num; i++){                    
                            if( (children.item(i).getNodeType()==Node.ELEMENT_NODE) ||
                                        (children.item(i).getNodeType()==Node.TEXT_NODE && 
                                        !toBeIgnored(children.item(i).getNodeValue())))
                            {
                                el= new Elem(children.item(i),level+1);
                                queue.enqueue(el);
                            }
                        }
                    }
                }
            }
        }
        catch (IOException|IllegalArgumentException ioe){
            if(file != null){
                System.out.println("File \""+file+"\" could not be opened.");
            }
            else{
                System.out.println("IO Exception happened.");
            }
            System.out.println("Program will be terminated.");
        }
        catch (Exception e){
            e.getMessage();
            System.out.println("SOMETHING WENT WRONG!");
		}
	}
}