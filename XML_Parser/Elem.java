import org.w3c.dom.*;

class Elem {
    Node node;
    int level;
    Elem next;
    
    Elem (Node node, int l) {
        this.node=node;
        this.level=l;
        this.next=null;
    }
}