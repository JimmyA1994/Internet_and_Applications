class FIFO {
    Elem front;
    Elem back;

    FIFO(){
        this.front=null;
        this.back=null;
    }

    Elem peak(){
        return front;
    }

    void enqueue(Elem x){
        x.next=back;
        this.back=x;
        if(this.front==null){
            front=x;
        }
    }

    Elem dequeue(){
        Elem temp=null;
        if(this.front!=null){    
            temp=front;                    
            if(back==front) {   //queue has only one element                
                back=null;
                front=null;                
            }
            else{               //queue has more than one elements                
                Elem curr=this.back;
                while(curr.next!=front) curr=curr.next;
                curr.next=null;
                front=curr;
            }
        }
        return temp;
    }

    boolean isEmpty(){
        return front==null && back==null;
    }
}